

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function ProjectsItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Project Name"
                          description="Name of the Project"
                          fieldName="title"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Description"
                          description="Description of the Project"
                          fieldName="description"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringArrayField
                          title="Technologies Used"
                          description="List of Technologies Used"
                          fieldName="technologiesUsed"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringArrayField>
                      
                      <FormDateField
                          title="Start Date"
                          description="Project Start Date"
                          fieldName="startDate"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="End Date"
                          description="Project End Date"
                          fieldName="endDate"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="projectURL"
                          namespace="ProjectsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default ProjectsItemsProps;
