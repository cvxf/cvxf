

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { MembershipsItemsProps } from './membershipsItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function MembershipsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Memberships</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Memberships</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <MembershipsItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default MembershipsProps;
