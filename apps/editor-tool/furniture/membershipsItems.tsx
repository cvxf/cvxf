

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function MembershipsItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="organization"
                          namespace="MembershipsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="position"
                          namespace="MembershipsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="startDate"
                          namespace="MembershipsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="endDate"
                          namespace="MembershipsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default MembershipsItemsProps;
