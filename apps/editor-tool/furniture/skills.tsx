

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function SkillsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Skills</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Skills</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                    <FormStringArrayField
                        title="Skills"
                        description="List of Candidate Skills"
                        fieldName="Skills"
                        namespace=""
                        
                        
                      >
                            <ChevronsRight size="16"  color="white"/>
                      </FormStringArrayField>
                    
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default SkillsProps;
