

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function LanguagesItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Language"
                          description="a sppoken or written Language"
                          fieldName="language"
                          namespace="LanguagesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Proficiency"
                          description="Level of proficiency in the spoken or written Language"
                          fieldName="proficiency"
                          namespace="LanguagesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default LanguagesItemsProps;
