

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function PublicationsItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="title"
                          namespace="PublicationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="publisher"
                          namespace="PublicationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="publicationDate"
                          namespace="PublicationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="url"
                          namespace="PublicationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default PublicationsItemsProps;
