

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function InterestsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Interests</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Interests</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                    <FormStringArrayField
                        title="Interests"
                        description="List of Candidate Interests"
                        fieldName="Interests"
                        namespace=""
                        
                        
                      >
                            <ChevronsRight size="16"  color="white"/>
                      </FormStringArrayField>
                    
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default InterestsProps;
