

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function VolunteerExperienceItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Organization"
                          description="Organization Volunteered Experience"
                          fieldName="organization"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Position"
                          description="Position in Organization"
                          fieldName="position"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Location"
                          description="Location of Organization"
                          fieldName="location"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Start Date"
                          description="Start Date at Organization"
                          fieldName="startDate"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="End Date"
                          description="End Date at Organization"
                          fieldName="endDate"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringArrayField
                          title="Activities"
                          description="Volunteer Activities"
                          fieldName="activities"
                          namespace="VolunteerExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringArrayField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default VolunteerExperienceItemsProps;
