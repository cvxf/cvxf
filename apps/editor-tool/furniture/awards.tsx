

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { AwardsItemsProps } from './awardsItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function AwardsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Awards</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Awards</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <AwardsItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default AwardsProps;
