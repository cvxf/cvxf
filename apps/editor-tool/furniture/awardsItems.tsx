

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function AwardsItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="title"
                          namespace="AwardsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="organization"
                          namespace="AwardsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="dateReceived"
                          namespace="AwardsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="description"
                          namespace="AwardsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default AwardsItemsProps;
