

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function ObjectiveProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Objective</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Candidate Career Objective</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                    <FormStringField
                        title="Objective"
                        description="Candidate Career Objective"
                        fieldName="Objective"
                        namespace=""
                        
                        multiline
                      >
                            <ChevronsRight size="16"  color="white"/>
                      </FormStringField>
                    
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default ObjectiveProps;
