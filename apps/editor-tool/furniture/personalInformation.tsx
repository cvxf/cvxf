

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
          User,
          AtSign,
          Phone,
          Home,
          Linkedin,
          Globe,
} from 'react-feather';

export function PersonalInformationProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Personal Information</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">Stores the candidate&#x27;s personal information</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                      <FormStringField
                          title="Full Name"
                          description="Your Full name including middle names"
                          fieldName="fullName"
                          namespace="PersonalInformation"
                          required
                          
                        >
                              <User size="16" color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="E-Mail Address"
                          description="Your E-Mail Address"
                          fieldName="email"
                          namespace="PersonalInformation"
                          required
                          
                        >
                              <AtSign size="16" color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Phone Number"
                          description="Your Phone number"
                          fieldName="phone"
                          namespace="PersonalInformation"
                          required
                          
                        >
                              <Phone size="16" color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Address"
                          description="Your Full address including Postal code"
                          fieldName="address"
                          namespace="PersonalInformation"
                          
                          multiline
                        >
                              <Home size="16" color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="LinkedIn Profile"
                          description="Your LinkedIn Profile"
                          fieldName="linkedInProfile"
                          namespace="PersonalInformation"
                          
                          
                        >
                              <Linkedin size="16" color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Website"
                          description="Your personal website"
                          fieldName="website"
                          namespace="PersonalInformation"
                          
                          
                        >
                              <Globe size="16" color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default PersonalInformationProps;
