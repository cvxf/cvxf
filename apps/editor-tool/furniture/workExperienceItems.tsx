

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function WorkExperienceItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Company Name"
                          description="Company worked for"
                          fieldName="company"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Position"
                          description="Position or Job Title held at the Company"
                          fieldName="position"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Location"
                          description="Location of Company"
                          fieldName="location"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Start Date"
                          description="Date Started at Company"
                          fieldName="startDate"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="End Date"
                          description="Last Date at Company"
                          fieldName="endDate"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringArrayField
                          title="Responsibilities"
                          description="Responsibilities of the Position"
                          fieldName="responsibilities"
                          namespace="WorkExperienceItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringArrayField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default WorkExperienceItemsProps;
