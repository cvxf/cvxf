

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { ReferencesItemsProps } from './referencesItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function ReferencesProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>References</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate References</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <ReferencesItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default ReferencesProps;
