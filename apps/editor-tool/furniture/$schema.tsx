

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function $schemaProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title></Card.Title>
        <Card.Subtitle className="mb-2 text-muted"></Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                    <FormStringField
                        title=""
                        description=""
                        fieldName="$schema"
                        namespace=""
                        
                        
                      >
                            <ChevronsRight size="16"  color="white"/>
                      </FormStringField>
                    
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default $schemaProps;
