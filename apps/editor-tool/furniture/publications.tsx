

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { PublicationsItemsProps } from './publicationsItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function PublicationsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Publications</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Publications</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <PublicationsItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default PublicationsProps;
