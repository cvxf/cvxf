/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import Link from 'next/link';
import { Nav } from 'react-bootstrap';
import {
          FileText,
          User,
          Target,
          Tool,
          Globe,
          Briefcase,
          Tag,
          BookOpen,
          Archive,
          Award,
          Coffee,
          UserCheck,
          FilePlus,
} from 'react-feather';
import { ScrollToButton } from 'ui-components';

export function SideBarLinks() {
  return (
    <Nav defaultActiveKey="/" className="flex-column">

<ScrollToButton toId="pagetop">
          <FileText size={17}   color="slateblue"/>
          &nbsp; My Resume</ScrollToButton>
          <Nav defaultActiveKey="/" className="flex-column subnav">

        <ScrollToButton toId="personalInformation">
              
          <User size={17} color="slateblue" />
          &nbsp; Personal Information</ScrollToButton>

        <ScrollToButton toId="summaryObjective">
              
          <Target size={17} color="slateblue" />
          &nbsp; Summary &amp; Objective</ScrollToButton>

        <ScrollToButton toId="skills">
              
          <Tool size={17} color="slateblue" />
          &nbsp; Skills</ScrollToButton>

        <ScrollToButton toId="languages">
              
          <Globe size={17} color="slateblue" />
          &nbsp; Languages</ScrollToButton>

        <ScrollToButton toId="workExperience">
              
          <Briefcase size={17} color="slateblue" />
          &nbsp; Work Experience</ScrollToButton>

        <ScrollToButton toId="projects">
              
          <Tag size={17} color="slateblue" />
          &nbsp; Projects</ScrollToButton>

        <ScrollToButton toId="education">
              
          <BookOpen size={17} color="slateblue" />
          &nbsp; Education</ScrollToButton>

        <ScrollToButton toId="certificates">
              
          <Archive size={17} color="slateblue" />
          &nbsp; Certificates</ScrollToButton>

        <ScrollToButton toId="awards">
              
          <Award size={17} color="slateblue" />
          &nbsp; Awards</ScrollToButton>

        <ScrollToButton toId="volunteerExperience">
              
          <Coffee size={17} color="slateblue" />
          &nbsp; Volunteer Experience</ScrollToButton>

        <ScrollToButton toId="references">
              
          <UserCheck size={17} color="slateblue" />
          &nbsp; References</ScrollToButton>

        <ScrollToButton toId="additionalInformation">
              
          <FilePlus size={17} color="slateblue" />
          &nbsp; Additional Information</ScrollToButton>

          </Nav>
</Nav>)
  
}
