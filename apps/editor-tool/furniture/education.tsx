

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { EducationItemsProps } from './educationItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function EducationProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Education</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Education</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <EducationItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default EducationProps;
