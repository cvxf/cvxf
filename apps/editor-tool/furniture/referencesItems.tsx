

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function ReferencesItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="name"
                          namespace="ReferencesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="title"
                          namespace="ReferencesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="company"
                          namespace="ReferencesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="phone"
                          namespace="ReferencesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Project Website"
                          description="Project Website (if applicable)"
                          fieldName="email"
                          namespace="ReferencesItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default ReferencesItemsProps;
