

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function HobbiesProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Hobbies</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Hobbies</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
                    <FormStringArrayField
                        title="Hobbies"
                        description="List of Candidate Hobbies"
                        fieldName="Hobbies"
                        namespace=""
                        
                        
                      >
                            <ChevronsRight size="16"  color="white"/>
                      </FormStringArrayField>
                    
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default HobbiesProps;
