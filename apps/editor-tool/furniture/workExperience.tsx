

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { WorkExperienceItemsProps } from './workExperienceItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function WorkExperienceProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Work Experience</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Work Experience</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <WorkExperienceItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default WorkExperienceProps;
