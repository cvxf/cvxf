

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { LanguagesItemsProps } from './languagesItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function LanguagesProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Languages</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Spoken Languages</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <LanguagesItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default LanguagesProps;
