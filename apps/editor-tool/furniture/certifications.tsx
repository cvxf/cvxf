

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { CertificationsItemsProps } from './certificationsItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function CertificationsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Certifications</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Certifications</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <CertificationsItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default CertificationsProps;
