

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { VolunteerExperienceItemsProps } from './volunteerExperienceItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function VolunteerExperienceProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Volunteer Experience</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Volunteer Experience</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <VolunteerExperienceItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default VolunteerExperienceProps;
