

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';

import { ProjectsItemsProps } from './projectsItems';


import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function ProjectsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Card.Title>Projects</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">List of Candidate Projects</Card.Subtitle>
        <Card.Text>
          
        </Card.Text>
        <Form>
        </Form>
        <ProjectsItemsProps />
      
      </Card.Body>
    </Card>

  </Container>;
}

export default ProjectsProps;
