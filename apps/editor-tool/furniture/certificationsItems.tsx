

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function CertificationsItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Certification"
                          description="Name of the Certification awarded"
                          fieldName="name"
                          namespace="CertificationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Issuing Organization"
                          description="Organization that issued the Certificate"
                          fieldName="organization"
                          namespace="CertificationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Date Issued"
                          description="Date the Certificate was awarded"
                          fieldName="dateReceived"
                          namespace="CertificationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="Date Valid Util"
                          description="Expiry Date of the Certificate"
                          fieldName="validUntil"
                          namespace="CertificationsItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default CertificationsItemsProps;
