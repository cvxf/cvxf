

/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/

import React, { useState, useEffect, useLayoutEffect } from "react";
import { Card, Container, Form } from 'react-bootstrap';
import { FormStringField, FormStringArrayField, FormDateField } from 'ui-components';



import {
  X,
  Check,
  Minus,
  ChevronsRight,
} from 'react-feather';

export function EducationItemsProps() {
  
  return <Container fluid className="p-1">
    <Card>
      <Card.Body>
        <Form>
                      <FormStringField
                          title="Institution"
                          description="Name of the Institution"
                          fieldName="institution"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Degree"
                          description="Degree Awarded"
                          fieldName="degree"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormStringField
                          title="Major"
                          description="Name of the Degree Major"
                          fieldName="major"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
                      <FormDateField
                          title="Start Date"
                          description="Date Started at Institution"
                          fieldName="startDate"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormDateField
                          title="End Date"
                          description="Last Date at Institution"
                          fieldName="endDate"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormDateField>
                      
                      <FormStringField
                          title="GPA"
                          description="Grade Point Average"
                          fieldName="GPA"
                          namespace="EducationItems"
                          
                          
                        >
                              <ChevronsRight size="16"  color="white"/>
                        </FormStringField>
                      
        </Form>
      
      </Card.Body>
    </Card>

  </Container>;
}

export default EducationItemsProps;
