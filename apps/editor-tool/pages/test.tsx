import styles from './index.module.css';

import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { Layout } from 'ui-components';

import React, { useState, useEffect, useLayoutEffect } from 'react';
import { Form, Card, Container, InputGroup, Badge } from 'react-bootstrap';

import { X, Check, Minus, ChevronsRight } from 'react-feather';

import { FileText, User } from 'react-feather';

export function TestPage() {
  const [SkillsItem, setSkillsItem] = useState('');
  const itemsList: string[] = [];
  const [SkillsItems, setSkillsItems] = useState(itemsList);

  useLayoutEffect(() => {
    if (sessionStorage.getItem('stateSkillsItems')) {
      setSkillsItem(sessionStorage.getItem('stateSkillsItem') || '');
      setSkillsItems(
        sessionStorage.getItem('stateSkillsItems')?.split(',') || []
      );
    } else {
      sessionStorage.setItem('stateSkillsItem', SkillsItem);
      sessionStorage.setItem('stateSkillsItema', SkillsItems.join(','));
    }
  }, []);

  useEffect(() => {
    sessionStorage.setItem('stateSkillsItem', SkillsItem);
    sessionStorage.setItem('stateSkillsItems', SkillsItems.join(','));
  }, [SkillsItem, SkillsItems]);

  const keydown = (e) => {
    if (e.which == 13) {
      e.preventDefault();
      setSkillsItems(SkillsItems.concat([SkillsItem]));
      setSkillsItem('');
    }
    return e;
  };

  const removeItem = (skill) => {
    setSkillsItems(SkillsItems.filter((e) => e !== skill));
  };

  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.css file.
   */
  return (
    <Layout sidebarLinks={<></>}>
      <Breadcrumb>
        <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
        <Breadcrumb.Item active>My Resume</Breadcrumb.Item>
      </Breadcrumb>
      <a className="anchor" id="pagetop"></a>
      <h1>
        <FileText size={42} color="slateblue" />
        &nbsp;Test Page
      </h1>
      &nbsp;
      <a className="anchor" id="personalInformation"></a>
      <h3>
        <User size={36} color="slateblue" />
        &nbsp;Personal Information
      </h3>
      <Container fluid className="p-1">
        <Card>
          <Card.Body>
            <Form>
              <Form.Group className="mb-3" controlId="SkillsItems">
                <Form.Label>Skill</Form.Label>
                <InputGroup hasValidation>
                  <InputGroup.Text id="inputGroupPrepend_SkillsItems">
                    <ChevronsRight size="16" color="slateblue" />
                  </InputGroup.Text>
                  <Form.Control
                    type=""
                    placeholder="Enter Skill"
                    value={SkillsItem}
                    onChange={(e) => {
                      setSkillsItem(e.target.value);
                    }}
                    onKeyDown={keydown}
                    isInvalid={!SkillsItems}
                  />
                  <InputGroup.Text id="inputGroupAppend_SkillsItems">
                    {SkillsItems ? (
                      <Check size="24" color="lime" />
                    ) : (
                      <X size="24" color="red" />
                    )}
                  </InputGroup.Text>
                </InputGroup>
                <Form.Text className="text-muted">
                  Skill (Technologies, Software etc)
                </Form.Text>
                <Form.Text as="p">
                  {SkillsItems.map((skill) => {
                    return (
                      <>
                        <span key={skill} className="badge bg-slateblue">
                          {skill}{' '}
                          <a
                            href="#"
                            onClick={(e) => removeItem(skill)}
                            className="text-white"
                          >
                            <X size={12} />
                          </a>
                        </span>
                        &nbsp;
                      </>
                    );
                  })}
                </Form.Text>
              </Form.Group>
            </Form>
          </Card.Body>
        </Card>
      </Container>
    </Layout>
  );
}

export default TestPage;
