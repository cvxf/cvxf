import { AppProps } from 'next/app';
import Head from 'next/head';
import './styles.css';
import { Navigation } from 'ui-components';
import 'bootstrap/dist/css/bootstrap.min.css';

const random = Math.random();

function CustomApp({ Component, pageProps }: AppProps) {
  console.log('Rendering app', random);

  return (
    <>
      <Head>
        <title>CVXF</title>
      </Head>
      <header>
        <Navigation></Navigation>
      </header>

      <Component {...pageProps} />
    </>
  );
}

export default CustomApp;
