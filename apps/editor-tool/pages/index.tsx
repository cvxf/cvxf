/***********************************************************************************
 * This file is generated 
 ***********************************************************************************/
import styles from './index.module.css';

import Breadcrumb from 'react-bootstrap/Breadcrumb';
import { Layout } from 'ui-components';
import { SideBarLinks } from '../furniture/sidebarlinks';

import { PersonalInformationProps } from "../furniture/personalInformation";
import { SummaryProps } from "../furniture/summary";
import { ObjectiveProps } from "../furniture/objective";
import { SkillsProps } from "../furniture/skills";
import { LanguagesProps } from "../furniture/languages";
import { WorkExperienceProps } from "../furniture/workExperience";
import { ProjectsProps } from "../furniture/projects";
import { EducationProps } from "../furniture/education";
import { CertificationsProps } from "../furniture/certifications";
import { AwardsProps } from "../furniture/awards";
import { VolunteerExperienceProps } from "../furniture/volunteerExperience";
import { ReferencesProps } from "../furniture/references";
import { InterestsProps } from "../furniture/interests";
import { HobbiesProps } from "../furniture/hobbies";
import { MembershipsProps } from "../furniture/memberships";

import {

          FileText,
          User,
          Target,
          Tool,
          Globe,
          Briefcase,
          Tag,
          BookOpen,
          Archive,
          Award,
          Coffee,
          UserCheck,
          FilePlus,

} from 'react-feather';

export function indexPage() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.css file.
   */
  return <Layout sidebarLinks={<SideBarLinks />}>
    <Breadcrumb>
      <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
      <Breadcrumb.Item active>My Resume</Breadcrumb.Item>
    </Breadcrumb>
    <a className="anchor" id="pagetop"></a>
    <h1><FileText size={42}  color="slateblue"/>&nbsp;My Resume</h1>
    
    &nbsp;
    <a className="anchor" id="personalInformation"></a>
    <h3><User size={36}  color="slateblue"/>&nbsp;Personal Information</h3>
    &nbsp;
      <PersonalInformationProps />
    &nbsp;
    <a className="anchor" id="summaryObjective"></a>
    <h3><Target size={36}  color="slateblue"/>&nbsp;Summary &amp; Objective</h3>
    &nbsp;
      <SummaryProps />
      <ObjectiveProps />
    &nbsp;
    <a className="anchor" id="skills"></a>
    <h3><Tool size={36}  color="slateblue"/>&nbsp;Skills</h3>
    &nbsp;
      <SkillsProps />
    &nbsp;
    <a className="anchor" id="languages"></a>
    <h3><Globe size={36}  color="slateblue"/>&nbsp;Languages</h3>
    &nbsp;
      <LanguagesProps />
    &nbsp;
    <a className="anchor" id="workExperience"></a>
    <h3><Briefcase size={36}  color="slateblue"/>&nbsp;Work Experience</h3>
    &nbsp;
      <WorkExperienceProps />
    &nbsp;
    <a className="anchor" id="projects"></a>
    <h3><Tag size={36}  color="slateblue"/>&nbsp;Projects</h3>
    &nbsp;
      <ProjectsProps />
    &nbsp;
    <a className="anchor" id="education"></a>
    <h3><BookOpen size={36}  color="slateblue"/>&nbsp;Education</h3>
    &nbsp;
      <EducationProps />
    &nbsp;
    <a className="anchor" id="certificates"></a>
    <h3><Archive size={36}  color="slateblue"/>&nbsp;Certificates</h3>
    &nbsp;
      <CertificationsProps />
    &nbsp;
    <a className="anchor" id="awards"></a>
    <h3><Award size={36}  color="slateblue"/>&nbsp;Awards</h3>
    &nbsp;
      <AwardsProps />
    &nbsp;
    <a className="anchor" id="volunteerExperience"></a>
    <h3><Coffee size={36}  color="slateblue"/>&nbsp;Volunteer Experience</h3>
    &nbsp;
      <VolunteerExperienceProps />
    &nbsp;
    <a className="anchor" id="references"></a>
    <h3><UserCheck size={36}  color="slateblue"/>&nbsp;References</h3>
    &nbsp;
      <ReferencesProps />
    &nbsp;
    <a className="anchor" id="additionalInformation"></a>
    <h3><FilePlus size={36}  color="slateblue"/>&nbsp;Additional Information</h3>
    &nbsp;
      <InterestsProps />
      <HobbiesProps />
      <MembershipsProps />
    &nbsp;
  </Layout>;
}

export default indexPage;
