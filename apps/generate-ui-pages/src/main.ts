import { compile } from 'handlebars';
import { readJsonFile, readTextFile, saveFile } from 'utils';

const INPUT_JSON_FILE = './assets/json/schema/cvxf_simplified.schema.json';
const SIDEBAR_LINKS_TEMPLATE =
  './assets/mustache/templates/sidebarlinks.template';
const PAGE_TEMPLATE = './assets/mustache/templates/page.template';
const PROPERTY_TEMPLATE = './assets/mustache/templates/property.template';
const OUTPUT_DIR = './apps/editor-tool/';
const PAGES_OUTPUT_DIR = `${OUTPUT_DIR}pages/`;
const FURNITURE_OUTPUT_DIR = `${OUTPUT_DIR}furniture/`;
const OUTPUT_FILE = `${FURNITURE_OUTPUT_DIR}sidebarlinks.tsx`;

const capitalise = (tocap: string): string =>
  tocap.charAt(0).toUpperCase() + tocap.slice(1);

const typeFlags = (type: string): boolean[] => {
  const arrayField = type === 'array';
  const singleField = type !== 'object' && !arrayField;
  return [arrayField, singleField];
};

interface propType {
  type: string;
  items?: {
    type: string;
  };
}

const calcFlags = (prop: propType): boolean[] => {
  let [arrayField, singleField] = typeFlags(prop.type);

  const [itemArrayField, itemSingleField] = prop.items
    ? typeFlags(prop.items.type)
    : [false, false];

  if (arrayField && itemSingleField) {
    singleField = true;
    arrayField = true;
  }

  return [arrayField, singleField, itemArrayField, itemSingleField];
};

async function main() {
  const jsonFilePath = INPUT_JSON_FILE;
  try {
    console.log('Going to Generate ui pages...');
    const jsonContent = await readJsonFile(jsonFilePath);
    const sbtemplate: string = await readTextFile(SIDEBAR_LINKS_TEMPLATE);
    const pageTemplate: string = await readTextFile(PAGE_TEMPLATE);
    const propTemplate: string = await readTextFile(PROPERTY_TEMPLATE);

    const sbtempl = compile(sbtemplate);
    const pageTempl = compile(pageTemplate);
    const propTempl = compile(propTemplate);

    console.log('Side Bar Links...');
    const output = sbtempl(jsonContent);

    await saveFile(OUTPUT_FILE, output);

    Object.keys(jsonContent.ui.pages).map(async (ele) => {
      const sections = jsonContent.ui.pages[ele].sections;
      const page = {
        id: ele,
        page: {
          ...jsonContent.ui.pages[ele],
          sections: Object.keys(sections).map((section) => {
            return {
              ...sections[section],
              properties: sections[section].properties.map((prop) => {
                return {
                  property: prop,
                  propertyId: capitalise(prop),
                };
              }),
            };
          }),
        },

        jsonContent,
      };

      console.log(ele);

      const pageOutput = pageTempl(page);

      await saveFile(`${PAGES_OUTPUT_DIR}${ele}.tsx`, pageOutput);
    });

    Object.keys(jsonContent.properties).map(async (ele) => {
      const propertyElement = jsonContent.properties[ele];

      const [arrayField, singleField, itemArrayField, itemSingleField] =
        calcFlags(propertyElement);

      if (!singleField && !arrayField) {
        Object.keys(propertyElement.properties).map((prop) => {
          console.log('is prop', prop, 'in', propertyElement.required);
          if (propertyElement.required.indexOf(prop) >= 0) {
            console.log('Yes');
            if (!propertyElement.properties[prop].ui) {
              propertyElement.properties[prop].ui = {};
            }
            propertyElement.properties[prop].ui.required = true;
          }
        });
      }

      if (propertyElement.properties) {
        propertyElement.properties = Object.keys(
          propertyElement.properties
        ).map((propName) => {
          const prop = propertyElement.properties[propName];
          const [arrayField, singleField] = typeFlags(prop.type);

          if (!prop.ui) {
            prop.ui = {};
          }
          prop.ui.controlName = 'FormStringField';

          if (prop.format === 'date') {
            prop.ui.controlName = 'FormDateField';
          }

          console.debug(ele, prop, arrayField, singleField);

          return {
            ...prop,
            arrayField,
            singleField,
            propName,
          };
        });
      }

      console.debug(
        ele,
        singleField,
        arrayField,
        itemSingleField,
        itemArrayField
      );

      if (singleField || (arrayField && itemSingleField)) {
        if (!propertyElement.ui) {
          propertyElement.ui = {};
        }
        propertyElement.ui.controlName = itemSingleField
          ? 'FormStringArrayField'
          : 'FormStringField';
      }

      const property = {
        id: ele,
        name: capitalise(ele),
        singleField,
        arrayField,
        itemArrayField,
        itemSingleField,
        showHeader: true,
        property: propertyElement,
        jsonContent,
      };

      console.log(property.name);

      const propOutput = propTempl(property);

      await saveFile(`${FURNITURE_OUTPUT_DIR}${ele}.tsx`, propOutput);

      if (propertyElement.items) {
        if (!itemSingleField) {
          if (propertyElement.items.properties) {
            propertyElement.items.properties = Object.keys(
              propertyElement.items.properties
            ).map((propName) => {
              const prop = propertyElement.items.properties[propName];
              const [arrayField, singleField] = typeFlags(prop.type);

              if (!prop.ui) {
                prop.ui = {};
              }
              prop.ui.controlName = arrayField
                ? 'FormStringArrayField'
                : 'FormStringField';

              if (prop.format === 'date') {
                prop.ui.controlName = 'FormDateField';
              }

              console.debug(ele, prop, arrayField, singleField);

              return {
                ...prop,
                arrayField,
                singleField,
                propName,
              };
            });
          }

          const property = {
            id: `${ele}Items`,
            name: `${capitalise(ele)}Items`,
            singleField: itemSingleField,
            arrayField: itemArrayField,
            itemField: true,
            property: propertyElement.items,
            jsonContent,
          };

          console.log(property.name);

          const propOutput = propTempl(property);

          await saveFile(`${FURNITURE_OUTPUT_DIR}${ele}Items.tsx`, propOutput);
        }
      }
    });
  } catch (error) {
    console.error('Error:', error);
  }
}

main();
