import {
  HandlerMap,
  PATH_SEP,
  RecursiveMapFunction,
  deQualify,
  handleElement,
  recursiveMap,
} from 'json-walker';
import { readJsonFile, saveJsonFile } from 'utils';

const INPUT_JSON_FILE = './assets/json/schema/cvxf_simplified.schema.json';
const OUTPUT_JSON_FILE = './assets/json/schema/cvxf.schema.json';

const ITEMS = 'items';

function getDefinitionsRoot(root: any): any {
  if (!root.definitions) {
    root.definitions = {};
  }
  return root.definitions;
}

function getTypeDefinitionsRoot(root: any): any {
  const definitions = getDefinitionsRoot(root);
  if (!definitions.types) {
    definitions.types = {};
  }
  return definitions.types;
}

function getObjectDefinitionsRoot(root: any): any {
  const definitions = getDefinitionsRoot(root);
  if (!definitions.objects) {
    definitions.objects = {};
  }
  return definitions.objects;
}

function getItemsParentName(qualified: string): string {
  return qualified.endsWith(`/${ITEMS}`)
    ? deQualify(qualified.substring(0, qualified.lastIndexOf(`/${ITEMS}`)))
    : deQualify(qualified);
}

function handleObjectType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  //console.log("handle object type", key);
  var baseName = deQualify(key);
  if (baseName === '') {
    return value;
  }

  if (baseName === ITEMS) {
    baseName = `${getItemsParentName(key)}Items`;
  }

  const types = getObjectDefinitionsRoot(root);

  types[`${baseName}`] = value;
  return {
    $ref: `#/definitions/objects/${baseName}`,
  };
}

function handleArrayType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  var baseName = deQualify(key);

  const types = getObjectDefinitionsRoot(root);

  types[`${baseName}`] = value;
  return {
    $ref: `#/definitions/objects/${baseName}`,
  };
}

function handlePrimativeType(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any
): any {
  const types = getTypeDefinitionsRoot(root);

  var baseName = deQualify(key);

  if (baseName === ITEMS) {
    baseName = `${getItemsParentName(key)}Items`;
  }

  types[`${baseName}Type`] = value;
  return {
    $ref: `#/definitions/types/${baseName}Type`,
  };
}

const objectHandlers: { [key: string]: RecursiveMapFunction } = {
  string: handlePrimativeType,
  boolean: handlePrimativeType,
  number: handlePrimativeType,
  object: handleObjectType,
  array: handleArrayType,
};

function handleObject(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any,
  handlers: HandlerMap
): any {
  const handler = objectHandlers[value.type];
  if (handler) return handler(key, value, parent, typename, root, handlers);

  return value;
}

const handlers: HandlerMap = {
  object: handleObject,
};

async function main() {
  const jsonFilePath = INPUT_JSON_FILE;
  try {
    console.log('Going to Generate compilcated schema...');
    const jsonContent = await readJsonFile(jsonFilePath);

    const mappedJson = recursiveMap(
      jsonContent,
      (key, value, parent, typename, root, handlers) => {
        if (deQualify(key) === 'ui') return undefined;
        if (key.startsWith('/definitions')) return value;
        return handleElement(key, value, parent, typename, root, handlers);
      },
      handlers
    );

    console.log('Mapped JSON:');

    await saveJsonFile(OUTPUT_JSON_FILE, mappedJson);
  } catch (error) {
    console.error('Error:', error);
  }
}

main();
