import { jsonWalker } from './json-walker';

describe('jsonWalker', () => {
  it('should work', () => {
    expect(jsonWalker()).toEqual('json-walker');
  });
});
