import { type } from 'os';

export const PATH_SEP = '/';

export function jsonWalker(): string {
  return 'json-walker';
}

export interface HandlerMap {
  [key: string]: RecursiveMapFunction;
}

export interface RecursiveMapFunction {
  (
    key: string,
    value: any,
    parent: any,
    typename: string,
    root: any,
    handlers: HandlerMap
  ): any;
}

export function recursiveMap(
  obj: any,
  fn: RecursiveMapFunction,
  handlers: HandlerMap = defaultHandlers,
  path: string = '',
  key: string = '',
  parent: any = undefined,
  root: any = obj
): any {
  var result = obj;
  var typename = typeof obj;
  const handlerMap: HandlerMap = {
    ...defaultHandlers,
    ...handlers,
  };
  if (Array.isArray(obj)) {
    var index = 0;
    result = [];
    obj.forEach((item) => {
      result.push(
        recursiveMap(item, fn, handlerMap, path, String(index++), obj, root)
      );
    });
  } else if (typeof obj === 'object' && obj !== null) {
    result = {};
    for (const key in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, key)) {
        const value = obj[key];
        result[key] = recursiveMap(
          value,
          fn,
          handlerMap,
          `${path}${PATH_SEP}${key}`,
          key,
          obj,
          root
        );
      }
    }
  }

  return fn(path, result, parent, typename, root, handlers);
}

export function handleObject(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any,
  handlers: HandlerMap
): any {
  return value;
}

export function handleArray(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any,
  handlers: HandlerMap
): any {
  return value;
}

export function handlePrimitave(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any,
  handlers: HandlerMap
): any {
  return value;
}

export function handleElement(
  key: string,
  value: any,
  parent: any,
  typename: string,
  root: any,
  handlers: HandlerMap
): any {
  return handlers[typename](key, value, parent, typename, root, handlers);
}

const defaultHandlers: HandlerMap = {
  object: handleObject,
  array: handleArray,
  boolean: handlePrimitave,
  string: handlePrimitave,
  number: handlePrimitave,
};

export function deQualify(qualified: string): string {
  return qualified.lastIndexOf(PATH_SEP) > -1
    ? qualified.substring(qualified.lastIndexOf(PATH_SEP) + 1)
    : qualified;
}
