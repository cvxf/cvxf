import ReactDOM from 'react-dom';
import { DatePickerProps, DatePickerState } from './props';
import { ReactInstance } from 'react';

export const handleHide = (
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void,
  hiddenInput: ReactInstance
) => {
  if (state.inputFocused) {
    return;
  }
  setState({ ...state, focused: false });
  if (props.onBlur) {
    const event = document.createEvent('CustomEvent');
    event.initEvent('Change Date', true, false);
    const node = ReactDOM.findDOMNode(hiddenInput);
    node?.dispatchEvent(event);
    props.onBlur(event);
  }
};
