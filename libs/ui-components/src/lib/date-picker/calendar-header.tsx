import { Button, Col, Container, Row } from 'react-bootstrap';
import { CalenderHeaderProps } from './props';
import {
  ChevronLeft,
  ChevronRight,
  ChevronsLeft,
  ChevronsRight,
} from 'react-feather';
import { MouseEvent } from 'react';

export function CalendarHeader(props: CalenderHeaderProps) {
  const displayingMinMonth = () => {
    if (!props.minDate) return false;
    if (!props.displayDate) return false;

    const displayDate = new Date(props.displayDate);
    const minDate = new Date(props.minDate);
    return (
      minDate.getFullYear() === displayDate.getFullYear() &&
      minDate.getMonth() === displayDate.getMonth()
    );
  };

  const displayingMaxMonth = () => {
    if (!props.maxDate) return false;
    if (!props.displayDate) return false;

    const displayDate = new Date(props.displayDate);
    const maxDate = new Date(props.maxDate);
    return (
      maxDate.getFullYear() === displayDate.getFullYear() &&
      maxDate.getMonth() === displayDate.getMonth()
    );
  };

  const handleClickPrevious = (e: MouseEvent<HTMLElement>, months: number) => {
    if (!props.displayDate) return;
    const newDisplayDate = new Date(props.displayDate);
    newDisplayDate.setDate(1);
    newDisplayDate.setMonth(newDisplayDate.getMonth() - months);
    if (props.onChange) {
      props.onChange(newDisplayDate);
    }
  };

  const handleClickNext = (e: MouseEvent<HTMLElement>, months: number) => {
    if (!props.displayDate) return;

    const newDisplayDate = new Date(props.displayDate);
    newDisplayDate.setDate(1);
    newDisplayDate.setMonth(newDisplayDate.getMonth() + months);
    if (props.onChange) {
      props.onChange(newDisplayDate);
    }
  };

  return (
    <div>
      <span
        style={{
          cursor: 'pointer',
          display: 'block',

          padding: '5px',
          float: 'left',
        }}
        onClick={(e) => handleClickPrevious(e, 12)}
        onMouseDown={(e) => e.preventDefault()}
      >
        <ChevronsLeft />
      </span>
      <span
        style={{
          cursor: 'pointer',
          display: 'block',

          padding: '5px',
          float: 'left',
        }}
        onClick={(e) => handleClickPrevious(e, 1)}
        onMouseDown={(e) => e.preventDefault()}
      >
        <ChevronLeft />
      </span>
      <span
        className="text-center"
        style={{
          display: 'block',
          width: '85px',
          padding: '5px',
          float: 'left',
        }}
      >
        {props.displayDate &&
          props.monthLabels &&
          props.monthLabels[props.displayDate?.getMonth()]}{' '}
        {props.displayDate && props.displayDate?.getFullYear()}
      </span>
      <span
        style={{
          cursor: 'pointer',
          display: 'block',
          padding: '5px',
          float: 'left',
        }}
        onClick={(e) => handleClickNext(e, 1)}
        onMouseDown={(e) => e.preventDefault()}
      >
        <ChevronRight />
      </span>
      <span
        style={{
          cursor: 'pointer',
          display: 'block',
          padding: '5px',
          float: 'right',
        }}
        onClick={(e) => handleClickNext(e, 12)}
        onMouseDown={(e) => e.preventDefault()}
      >
        <ChevronsRight />
      </span>
    </div>
  );
}
