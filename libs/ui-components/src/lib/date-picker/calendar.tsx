import { MouseEvent } from 'react';
import { Button } from 'react-bootstrap';
import { CalendarProps } from './props';

const daysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

export function Calendar(props: CalendarProps) {
  const handleClick = (e: MouseEvent<HTMLElement>) => {
    if (e.currentTarget) {
      const day = e.currentTarget.getAttribute('data-day');
      const newSelectedDate = setTimeToNoon(
        new Date(props.displayDate ? props.displayDate : new Date())
      );
      newSelectedDate.setDate(day ? Number(day) : 0);
      if (props.onChange) {
        props.onChange(newSelectedDate);
      }
    }
  };

  const handleClickToday = () => {
    const newSelectedDate = setTimeToNoon(new Date());
    if (props.onChange) {
      props.onChange(newSelectedDate);
    }
  };

  const setTimeToNoon = (date: Date) => {
    date.setHours(12);
    date.setMinutes(0);
    date.setSeconds(0);
    date.setMilliseconds(0);
    return date;
  };

  const getWeekNumber = (date: Date) => {
    const target = new Date(date.valueOf());
    const dayNr = (date.getDay() + 6) % 7;
    target.setDate(target.getDate() - dayNr + 3);
    const firstThursday = target.valueOf();
    target.setMonth(0, 1);
    if (target.getDay() !== 4) {
      target.setMonth(0, 1 + ((4 - target.getDay() + 7) % 7));
    }
    return 1 + Math.ceil((firstThursday - target.valueOf()) / 604800000);
  };

  const currentDate = setTimeToNoon(new Date());
  const selectedDate = props.selectedDate
    ? setTimeToNoon(new Date(props.selectedDate))
    : null;
  const minDate = props.minDate ? setTimeToNoon(new Date(props.minDate)) : null;
  const maxDate = props.maxDate ? setTimeToNoon(new Date(props.maxDate)) : null;
  const year = props.displayDate?.getFullYear();
  const month = props.displayDate?.getMonth();
  const firstDay = year && month ? new Date(year, month, 1) : null;
  const startingDay =
    props.weekStartsOn && firstDay
      ? props.weekStartsOn > 1
        ? firstDay.getDay() - props.weekStartsOn + 7
        : props.weekStartsOn === 1
        ? firstDay.getDay() === 0
          ? 6
          : firstDay.getDay() - 1
        : firstDay.getDay()
      : null;
  const showWeeks = props.showWeeks;

  let monthLength = month ? daysInMonth[month] : 31;
  if (month === 1) {
    if (year && ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0)) {
      monthLength = 29;
    }
  }

  const weeks = [];
  let day = 1;
  for (let i = 0; i < 9; i++) {
    const week = [];
    for (let j = 0; j <= 6; j++) {
      if (
        day <= monthLength &&
        (i > 0 || j >= (startingDay ? startingDay : 1))
      ) {
        let className = undefined;
        const date = new Date(
          year ? year : 1970,
          month ? month : 0,
          day,
          12,
          0,
          0,
          0
        ).toISOString();
        const beforeMinDate = minDate && Date.parse(date) < minDate.valueOf();
        const afterMinDate = maxDate && Date.parse(date) > maxDate.valueOf();
        let clickHandler: undefined | ((e: MouseEvent<HTMLElement>) => void) =
          handleClick;
        const style = {
          cursor: 'pointer',
          padding: props.cellPadding,
          borderRadius: props.roundedCorners ? 5 : 0,
        };

        if (beforeMinDate || afterMinDate) {
          className = 'text-muted';
          clickHandler = undefined;
          style.cursor = 'default';
        } else if (Date.parse(date) === selectedDate?.valueOf()) {
          className = 'bg-primary';
        } else if (Date.parse(date) === currentDate?.valueOf()) {
          className = 'text-primary';
        }

        week.push(
          <td key={j} style={style} className={className}>
            <span
              data-day={day}
              onMouseDown={(e) => e.preventDefault()}
              onClick={clickHandler}
            >
              {day}
            </span>
          </td>
        );
        day++;
      } else {
        week.push(<td key={j} />);
      }
    }

    if (showWeeks) {
      const weekNum = getWeekNumber(
        new Date(year ? year : 1970, month ? month : 0, day - 1, 12, 0, 0, 0)
      );
      week.unshift(
        <td
          key={7}
          style={{
            padding: props.cellPadding,
            fontSize: '0.8em',
            color: 'darkgrey',
          }}
          className="text-muted"
        >
          {weekNum}
        </td>
      );
    }

    weeks.push(<tr key={i}>{week}</tr>);
    if (day > monthLength) {
      for (let t = i; t < 5; t++) {
        weeks.push(
          <tr key={t + 1}>
            <td
              style={{
                padding: props.cellPadding,
                borderRadius: props.roundedCorners ? 5 : 0,
              }}
              className={'text-muted'}
            >
              <span>&nbsp;</span>
            </td>
          </tr>
        );
      }
      break;
    }
  }

  const weekColumn = showWeeks ? (
    <td
      className="text-muted current-week"
      style={{ padding: props.cellPadding }}
    />
  ) : null;

  return (
    <table className="text-center">
      <thead>
        <tr>
          {weekColumn}
          {props.dayLabels &&
            props.dayLabels.map((label, index) => {
              return (
                <td
                  key={index}
                  className="text-muted"
                  style={{ padding: props.cellPadding }}
                >
                  <small>{label}</small>
                </td>
              );
            })}
        </tr>
      </thead>
      <tbody>{weeks}</tbody>
      {props.showTodayButton && (
        <tfoot>
          <tr>
            <td
              colSpan={props.dayLabels ? props.dayLabels.length : 1}
              style={{ paddingTop: '9px' }}
            >
              <Button className="u-today-button" onClick={handleClickToday}>
                {props.todayButtonLabel}
              </Button>
            </td>
          </tr>
        </tfoot>
      )}
    </table>
  );
}
