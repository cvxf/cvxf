import { ChangeEvent, MouseEvent } from 'react';
import { DatePickerProps, DatePickerState } from './props';
import { makeDateValues } from './make-date-values';

export const clear = (
  e: MouseEvent<HTMLSpanElement> | ChangeEvent<HTMLSpanElement>,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void
) => {
  if (props.onClear) {
    props.onClear();
  } else {
    setState({
      ...makeDateValues('', props, state),
      placeholder: '',
      separator: '/',
      inputFocused: false,
      focused: false,
      dayLabels: [],
      mouseDown: undefined,
      show: undefined,
    });
  }

  if (props.onChange) {
    props.onChange('', '');
  }
};
