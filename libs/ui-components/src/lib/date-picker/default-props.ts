let instanceCount = 0;

export const getDefaultProps = () => {
  const language =
    typeof window !== 'undefined' && window.navigator
      ? (window.navigator.language || '').toLowerCase()
      : '';
  const dateFormat =
    !language || language === 'en-us' ? 'MM/DD/YYYY' : 'DD/MM/YYYY';
  return {
    cellPadding: '5px',
    dayLabels: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    monthLabels: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],
    clearButtonElement: '×',
    previousButtonElement: '<',
    nextButtonElement: '>',
    calendarPlacement: 'bottom',
    dateFormat: dateFormat,
    showClearButton: true,
    autoFocus: false,
    disabled: false,
    showTodayButton: false,
    todayButtonLabel: 'Today',
    autoComplete: 'on',
    showWeeks: false,
    instanceCount: instanceCount++,
    style: {
      width: '100%',
    },
    roundedCorners: false,
    noValidate: false,
  };
};
