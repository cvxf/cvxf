import {
  useState,
  useRef,
  useEffect,
  useLayoutEffect,
  useMemo,
  MouseEvent,
} from 'react';
import { Form, InputGroup, Overlay, Popover } from 'react-bootstrap';

import { DatePickerProps, DatePickerState } from './props';
import { CalendarHeader } from './calendar-header';
import { Calendar } from './calendar';
import { getDefaultProps } from './default-props';
import { getInitialState } from './get-init-state';
import { onChangeDate, onChangeMonth } from './on-change-month-date';
import { handleKeyDown } from './handle-key-down';
import { handleBlur, handleFocus } from './handle-focus-blur';
import { handleInputChange } from './handle-change-input';
import { clear } from './clear';

export function DatePicker(inputProps: DatePickerProps) {
  const refs = {
    input: useRef(null),
    hiddenInput: useRef(null),
    overlayContainer: useRef(null),
  };

  const props: DatePickerProps = useMemo(() => {
    return {
      ...getDefaultProps(),
      ...inputProps,
    };
  }, [inputProps]);

  const [state, setStateReal] = useState({
    inputValue: undefined,
    displayDate: undefined,
    selectedDate: undefined,
    placeholder: '',
    separator: '/',
    inputFocused: false,
    focused: false,
    dayLabels: [],
    mouseDown: undefined,
    show: undefined,
  } as DatePickerState);
  const setState = (state: DatePickerState) => {
    setStateReal(state);
  };
  console.debug('first settings', state);

  useEffect(() => {
    const initState: DatePickerState = getInitialState(props, {
      inputValue: undefined,
      displayDate: undefined,
      selectedDate: undefined,
      placeholder: '',
      separator: '/',
      inputFocused: false,
      focused: false,
      dayLabels: [],
      mouseDown: undefined,
      show: undefined,
    } as DatePickerState);
    setState(initState);
  }, [props]);

  const calendarHeader = (
    <CalendarHeader
      previousButtonElement={props.previousButtonElement}
      nextButtonElement={props.nextButtonElement}
      displayDate={state.displayDate}
      minDate={props.minDate}
      maxDate={props.maxDate}
      onChange={(date) => {
        onChangeMonth(date, props, state, setState);
      }}
      monthLabels={props.monthLabels}
      dateFormat={props.dateFormat}
    />
  );

  const control = (
    <Form.Control
      onKeyDown={(e) =>
        handleKeyDown(e, props, state, setState, refs.hiddenInput)
      }
      value={state.inputValue || ''}
      required={props.required}
      ref={refs.input}
      type="text"
      autoFocus={props.autoFocus}
      disabled={props.disabled}
      placeholder={state.focused ? props.dateFormat : state.placeholder}
      onFocus={(e) => handleFocus(props, state, setState, refs.hiddenInput)}
      onBlur={(e) => handleBlur(props, state, setState)}
      onChange={(e) => handleInputChange(e, props, state, setState, refs.input)}
      onClick={(e) => setState({ ...state, show: !state.show })}
      onMouseDown={(e) => setState({ ...state, mouseDown: true })}
      onMouseUp={(e) => setState({ ...state, mouseDown: false })}
      autoComplete={props.autoComplete}
      onInvalid={props.onInvalid}
    />
  );

  const calendar = (
    <Calendar
      cellPadding={props.cellPadding}
      selectedDate={state.selectedDate}
      displayDate={state.displayDate}
      onChange={(e) => onChangeDate(e, props, state, setState, refs.input)}
      dayLabels={state.dayLabels}
      weekStartsOn={props.weekStartsOn}
      showTodayButton={props.showTodayButton}
      todayButtonLabel={props.todayButtonLabel}
      minDate={props.minDate}
      maxDate={props.maxDate}
      roundedCorners={props.roundedCorners}
      showWeeks={props.showWeeks}
      dateFormat={props.dateFormat}
    />
  );

  return (
    <>
      {control}

      <Overlay
        rootClose={true}
        //onHide={handleHide}
        show={state.show}
        //container={() =>
        //  props.calendarContainer || ReactDOM.findDOMNode(refs.overlayContainer)
        //}
        //placement={state.calendarPlacement}
        //delayHide={200}
        target={refs.input}
      >
        <Popover
          id={`date-picker-popover-${props.instanceCount}`}
          className="date-picker-popover"
        >
          {calendarHeader}
          {calendar}
        </Popover>
      </Overlay>
      <div ref={refs.overlayContainer} style={{ position: 'relative' }} />
      <input
        ref={refs.hiddenInput}
        type="hidden"
        id={props.id}
        name={props.name}
        //value={state.value || ''}
        //data-formattedvalue={state.value ? state.inputValue : ''}
      />

      {false && props.showClearButton && !props.customControl && (
        <InputGroup.Text
          onClick={
            props.disabled
              ? undefined
              : (e: MouseEvent<HTMLElement>) => clear(e, props, state, setState)
          }
          style={{
            cursor:
              state.inputValue && !props.disabled ? 'pointer' : 'not-allowed',
          }}
        >
          <div
            style={{
              opacity: state.inputValue && !props.disabled ? 1 : 0.5,
            }}
          >
            {props.clearButtonElement}
          </div>
        </InputGroup.Text>
      )}
      {props.children}
    </>
  );
}
