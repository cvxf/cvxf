import { DatePickerProps, DatePickerState } from './props';

export const getSeperator = (
  props: DatePickerProps,
  state: DatePickerState
): string => {
  const dateFormatMatch = props.dateFormat?.match(/[^A-Z]/);

  const dateFormatSeperator = dateFormatMatch ? dateFormatMatch[0] : '/';

  const separator = state.separator ? state.separator : dateFormatSeperator;

  return separator;
};

export const makeInputValueString = (
  date: Date,
  props: DatePickerProps,
  state: DatePickerState
): string => {
  const month = date.getMonth() + 1;
  const day = date.getDate();

  const separator = getSeperator(props, state);

  if (props.dateFormat?.match(/MM.DD.YYYY/)) {
    return (
      (month > 9 ? month : `0${month}`) +
      separator +
      (day > 9 ? day : `0${day}`) +
      separator +
      date.getFullYear()
    );
  } else if (props.dateFormat?.match(/DD.MM.YYYY/)) {
    return (
      (day > 9 ? day : `0${day}`) +
      separator +
      (month > 9 ? month : `0${month}`) +
      separator +
      date.getFullYear()
    );
  }
  return (
    date.getFullYear() +
    separator +
    (month > 9 ? month : `0${month}`) +
    separator +
    (day > 9 ? day : `0${day}`)
  );
};

export const makeInputValueFromString = (
  inputValue: string,
  props: DatePickerProps,
  state: DatePickerState
): Date => {
  if (props.dateFormat?.match(/MM.DD.YYYY/)) {
    const month = Number(inputValue.substring(0, 2)) - 1;
    const day = Number(inputValue.substring(3, 5));
    const year = Number(inputValue.substring(6, 10));
    return new Date(year, month, day, 12, 0, 0, 0);
  } else if (props.dateFormat?.match(/DD.MM.YYYY/)) {
    const day = Number(inputValue.substring(0, 2));
    const month = Number(inputValue.substring(3, 5)) - 1;
    const year = Number(inputValue.substring(6, 10));
    return new Date(year, month, day, 12, 0, 0, 0);
  }
  const month = Number(inputValue.substring(5, 7)) - 1;
  const day = Number(inputValue.substring(8, 10));
  const year = Number(inputValue.substring(0, 4));
  return new Date(year, month, day, 12, 0, 0, 0);
};
