import { makeDateValues } from './make-date-values';
import { makeInputValueFromString } from './make-input-string-value';
import { DatePickerProps, DatePickerState } from './props';

export const getInitialState = (
  props: DatePickerProps,
  state: DatePickerState
) => {
  if (props.value && props.defaultValue) {
    throw new Error(
      "Conflicting DatePicker properties 'value' and 'defaultValue'"
    );
  }
  const inputValue = props.value || props.defaultValue;
  console.debug('initstate value', inputValue);
  const inputDate = inputValue
    ? makeInputValueFromString(inputValue, props, state).toISOString()
    : null;
  const result: DatePickerState = {
    ...makeDateValues(inputDate, props, state),
    placeholder: '',
    separator: '/',
    inputFocused: false,
    focused: false,
    dayLabels: [],
    mouseDown: undefined,
    show: undefined,
    inputValue,
  };
  if (props.weekStartsOn && props.dayLabels && props.weekStartsOn > 1) {
    result.dayLabels = props.dayLabels
      .slice(props.weekStartsOn)
      .concat(props.dayLabels.slice(0, props.weekStartsOn));
  } else if (
    props.weekStartsOn &&
    props.dayLabels &&
    props.weekStartsOn === 1
  ) {
    result.dayLabels = props.dayLabels
      .slice(1)
      .concat(props.dayLabels.slice(0, 1));
  } else {
    result.dayLabels = props.dayLabels;
  }
  result.focused = false;
  result.inputFocused = false;
  result.placeholder = props.placeholder || props.dateFormat;
  if (props.dateFormat) {
    const matches = props.dateFormat.match(/[^A-Z]/);
    if (matches && matches.length > 0) {
      result.separator = matches[0];
    }
  }
  console.debug('initstate result', result.inputValue);
  return result;
};
