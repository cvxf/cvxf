import { DatePickerProps } from './props';

export const getCalendarPlacement = (props: DatePickerProps) => {
  if (typeof props.calendarPlacement == 'function') {
    return props.calendarPlacement();
  } else {
    return props.calendarPlacement;
  }
};
