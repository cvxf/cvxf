import { MutableRefObject } from 'react';
import { DatePickerProps, DatePickerState } from './props';
import { getCalendarPlacement } from './get-calendar-placement';
import ReactDOM from 'react-dom';

export const handleFocus = (
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void,
  hiddenInput: MutableRefObject<null>
) => {
  if (state.focused === true) {
    return;
  }

  const placement = getCalendarPlacement(props);

  const show = state.mouseDown ? {} : { show: true };

  setState({
    ...state,
    inputFocused: true,
    focused: true,

    calendarPlacement: placement,
    ...show,
  });

  if (props.onFocus) {
    const event = document.createEvent('CustomEvent');
    event.initEvent('Change Date', true, false);
    const node = ReactDOM.findDOMNode(hiddenInput.current);
    node?.dispatchEvent(event);
    props.onFocus(event);
  }
};

export const handleBlur = (
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void
) => {
  console.debug('bluring');
  setState({ ...state, inputFocused: false, focused: false, show: false });
};
