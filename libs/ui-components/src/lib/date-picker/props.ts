import { ReactNode } from 'react';

export interface CalenderHeaderProps {
  displayDate?: Date;
  minDate?: string;
  maxDate?: string;
  onChange?: (date: Date) => void;
  monthLabels?: string[];
  previousButtonElement?: ReactNode;
  nextButtonElement?: ReactNode;
  dateFormat?: string;
}

export interface CalendarProps {
  selectedDate?: Date;
  displayDate?: Date;
  minDate?: string;
  maxDate?: string;
  onChange?: (date: Date) => void;
  dayLabels?: string[];
  cellPadding?: string;
  weekStartsOn?: number;
  showTodayButton?: boolean;
  todayButtonLabel?: string;
  roundedCorners?: boolean;
  showWeeks?: boolean;
  dateFormat?: string;
}

export interface DatePickerProps {
  defaultValue?: string;
  value?: string;
  required?: boolean;
  className?: string;
  style?: object;
  minDate?: string;
  maxDate?: string;
  cellPadding?: string;
  autoComplete?: string;
  placeholder?: string;
  dayLabels?: string[];
  monthLabels?: string[];
  onChange?: (x?: string, y?: string) => void;
  onClear?: () => void;
  onBlur?: (e: CustomEvent) => void;
  onFocus?: (e: CustomEvent) => void;
  autoFocus?: boolean;
  disabled?: boolean;
  weekStartsOn?: number;
  clearButtonElement?: ReactNode;
  showClearButton?: boolean;
  previousButtonElement?: string | ReactNode;
  nextButtonElement?: string | ReactNode;
  calendarPlacement?: string | (() => string);
  dateFormat?: string; // 'MM/DD/YYYY', 'DD/MM/YYYY', 'YYYY/MM/DD', 'DD-MM-YYYY'
  bsClass?: string;
  bsSize?: string;
  calendarContainer?: object;
  id?: string;
  name?: string;
  showTodayButton?: boolean;
  todayButtonLabel?: string;
  instanceCount?: number;
  customControl?: object;
  roundedCorners?: boolean;
  showWeeks?: boolean;
  children?: ReactNode | ReactNode[];
  onInvalid?: () => void;
  noValidate?: boolean;
}

export interface DatePickerState {
  mouseDown: boolean | undefined;
  show: boolean | undefined;
  inputValue?: any;
  displayDate?: any;
  selectedDate?: any;
  placeholder?: string;
  separator?: string;
  inputFocused?: boolean;
  focused?: boolean;
  dayLabels?: string[];
  calendarPlacement?: string | (() => string);
  value?: string | null;
}
