import { KeyboardEvent, MutableRefObject } from 'react';
import { DatePickerProps, DatePickerState } from './props';
import ReactDOM from 'react-dom';

export const handleKeyDown = (
  e: KeyboardEvent<any>,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void,
  hiddenInput: MutableRefObject<null>
) => {
  if (e.which === 9 && state.inputFocused) {
    setState({ ...state, focused: false });

    if (props.onBlur) {
      const event = document.createEvent('CustomEvent');
      event.initEvent('Change Date', true, false);
      const node = ReactDOM.findDOMNode(hiddenInput.current);
      node?.dispatchEvent(event);
      props.onBlur(event);
    }
  }
};
