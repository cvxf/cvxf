import { makeInputValueString } from './make-input-string-value';
import { DatePickerProps, DatePickerState } from './props';

export function makeDateValues(
  isoString: string | null,
  props: DatePickerProps,
  state: DatePickerState
) {
  let displayDate;
  const selectedDate = isoString
    ? new Date(`${isoString.slice(0, 10)}T12:00:00.000Z`)
    : null;
  const minDate = props.minDate
    ? new Date(`${props.minDate.slice(0, 10)}T12:00:00.000Z`)
    : null;
  const maxDate = props.maxDate
    ? new Date(`${props.maxDate.slice(0, 10)}T12:00:00.000Z`)
    : null;

  const inputValue = isoString
    ? makeInputValueString(
        selectedDate ? selectedDate : new Date(),
        props,
        state
      )
    : null;
  if (selectedDate) {
    displayDate = new Date(selectedDate);
  } else {
    const today = new Date(
      `${new Date().toISOString().slice(0, 10)}T12:00:00.000Z`
    );
    if (minDate && minDate.valueOf() >= today.valueOf()) {
      displayDate = minDate;
    } else if (maxDate && maxDate.valueOf() <= today.valueOf()) {
      displayDate = maxDate;
    } else {
      displayDate = today;
    }
  }
  console.debug('selected date', selectedDate, isoString, inputValue);
  return {
    value: selectedDate ? selectedDate.toISOString() : null,
    displayDate: displayDate,
    selectedDate: selectedDate,
    //inputValue: inputValue,
  };
}
