import { MutableRefObject } from 'react';
import { makeInputValueString } from './make-input-string-value';
import { DatePickerProps, DatePickerState } from './props';
import ReactDOM from 'react-dom';

export const onChangeMonth = (
  newDisplayDate: Date,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void
) => {
  setState({ ...state, displayDate: newDisplayDate });
};

export const onChangeDate = (
  newSelectedDate: Date,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void,
  input: MutableRefObject<null>
) => {
  const inputValue = makeInputValueString(newSelectedDate, props, state);
  setState({
    ...state,
    inputValue: inputValue,
    selectedDate: newSelectedDate,
    displayDate: newSelectedDate,
    value: newSelectedDate.toISOString(),
    focused: true,
    show: false,
  });

  console.debug('change date', newSelectedDate, inputValue);

  console.debug('sending blur event?');
  const event = document.createEvent('CustomEvent');
  event.initEvent('Change Date', true, false);
  if (props.onBlur) {
    props.onBlur(event);
  }

  if (props.onChange) {
    props.onChange(newSelectedDate.toISOString(), inputValue);
  }
};
