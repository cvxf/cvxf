import { ChangeEvent, MutableRefObject } from 'react';
import ReactDOM from 'react-dom';
import { DatePickerProps, DatePickerState } from './props';
import { clear } from './clear';
import { handleBadInput } from './handle-bad-input';

export const handleInputChange = (
  e: ChangeEvent<any>,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void,
  input: MutableRefObject<null>
) => {
  const originalValue = ReactDOM.findDOMNode(input.current)?.nodeValue;
  const inputValue = originalValue
    ?.replace(/(-|\/\/)/g, state.separator ? state.separator : '/')
    .slice(0, 10);

  if (!inputValue) {
    clear(e, props, state, setState);
    return;
  }

  let month, day, year;
  if (props.dateFormat?.match(/MM.DD.YYYY/)) {
    if (!inputValue.match(/[0-1][0-9].[0-3][0-9].[1-2][0-9][0-9][0-9]/)) {
      return handleBadInput(
        originalValue ? originalValue : '',
        props,
        state,
        setState
      );
    }

    month = inputValue.slice(0, 2).replace(/[^0-9]/g, '');
    day = inputValue.slice(3, 5).replace(/[^0-9]/g, '');
    year = inputValue.slice(6, 10).replace(/[^0-9]/g, '');
  } else if (props.dateFormat?.match(/DD.MM.YYYY/)) {
    if (!inputValue.match(/[0-3][0-9].[0-1][0-9].[1-2][0-9][0-9][0-9]/)) {
      return handleBadInput(
        originalValue ? originalValue : '',
        props,
        state,
        setState
      );
    }

    day = inputValue.slice(0, 2).replace(/[^0-9]/g, '');
    month = inputValue.slice(3, 5).replace(/[^0-9]/g, '');
    year = inputValue.slice(6, 10).replace(/[^0-9]/g, '');
  } else {
    if (!inputValue.match(/[1-2][0-9][0-9][0-9].[0-1][0-9].[0-3][0-9]/)) {
      return handleBadInput(
        originalValue ? originalValue : '',
        props,
        state,
        setState
      );
    }

    year = inputValue.slice(0, 4).replace(/[^0-9]/g, '');
    month = inputValue.slice(5, 7).replace(/[^0-9]/g, '');
    day = inputValue.slice(8, 10).replace(/[^0-9]/g, '');
  }

  const monthInteger = parseInt(month, 10);
  const dayInteger = parseInt(day, 10);
  const yearInteger = parseInt(year, 10);
  if (monthInteger > 12 || dayInteger > 31) {
    return handleBadInput(
      originalValue ? originalValue : '',
      props,
      state,
      setState
    );
  }

  if (
    !isNaN(monthInteger) &&
    !isNaN(dayInteger) &&
    !isNaN(yearInteger) &&
    monthInteger <= 12 &&
    dayInteger <= 31 &&
    yearInteger > 999
  ) {
    const selectedDate = new Date(
      yearInteger,
      monthInteger - 1,
      dayInteger,
      12,
      0,
      0,
      0
    );
    setState({
      ...state,
      selectedDate: selectedDate,
      displayDate: selectedDate,
      value: selectedDate.toISOString(),
    });

    if (props.onChange) {
      props.onChange(selectedDate.toISOString(), inputValue);
    }
  }

  setState({ ...state, inputValue: inputValue });
};
