import { DatePickerProps, DatePickerState } from './props';

export const handleBadInput = (
  originalValue: string,
  props: DatePickerProps,
  state: DatePickerState,
  setState: (state: DatePickerState) => void
) => {
  const parts = originalValue
    ? originalValue
        .replace(new RegExp(`[^0-9${state.separator}]`), '/')
        .split(state.separator ? state.separator : '/')
    : [];

  if (
    props.dateFormat?.match(/MM.DD.YYYY/) ||
    props.dateFormat?.match(/DD.MM.YYYY/)
  ) {
    if (parts[0] && parts[0].length > 2) {
      parts[1] = parts[0].slice(2) + (parts[1] || '');
      parts[0] = parts[0].slice(0, 2);
    }
    if (parts[1] && parts[1].length > 2) {
      parts[2] = parts[1].slice(2) + (parts[2] || '');
      parts[1] = parts[1].slice(0, 2);
    }
    if (parts[2]) {
      parts[2] = parts[2].slice(0, 4);
    }
  } else {
    if (parts[0] && parts[0].length > 4) {
      parts[1] = parts[0].slice(4) + (parts[1] || '');
      parts[0] = parts[0].slice(0, 4);
    }
    if (parts[1] && parts[1].length > 2) {
      parts[2] = parts[1].slice(2) + (parts[2] || '');
      parts[1] = parts[1].slice(0, 2);
    }
    if (parts[2]) {
      parts[2] = parts[2].slice(0, 2);
    }
  }
  setState({ ...state, inputValue: parts.join(state.separator) });
};
