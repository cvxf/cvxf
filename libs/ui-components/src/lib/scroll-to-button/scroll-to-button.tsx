import Link from 'next/link';
import { scrollTo } from '../scrollTo';
import styles from './scroll-to-button.module.css';
import { ReactNode } from 'react';

/* eslint-disable-next-line */
export interface ScrollToButtonProps {
  toId: string;
  toRef?: undefined;
  duration?: number;
  children: ReactNode;
}

export function ScrollToButton(props: ScrollToButtonProps) {
  const handleClick = () => scrollTo(props.toId, props.toRef, props.duration);

  return (
    <Link
      className="text-black nav-link"
      role="button"
      href="#"
      onClick={handleClick}
    >
      {props.children}
    </Link>
  );
}

export default ScrollToButton;
