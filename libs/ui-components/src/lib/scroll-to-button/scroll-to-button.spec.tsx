import { render } from '@testing-library/react';

import ScrollToButton from './scroll-to-button';

describe('ScrollToButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ScrollToButton />);
    expect(baseElement).toBeTruthy();
  });
});
