import { render } from '@testing-library/react';

import FormFieldLayout from './form-field-layout';

describe('FormFieldLayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FormFieldLayout />);
    expect(baseElement).toBeTruthy();
  });
});
