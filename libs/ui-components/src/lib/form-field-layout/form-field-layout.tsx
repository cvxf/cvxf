import styles from './form-field-layout.module.css';

import React, { ReactNode } from 'react';
import { Form, InputGroup } from 'react-bootstrap';

import { X, Check } from 'react-feather';

/* eslint-disable-next-line */
export interface FormFieldLayoutProps {
  title: string;
  description: string;
  fieldName: string;
  children: ReactNode;
  icon: ReactNode;
  valid: boolean;
  formText?: ReactNode;
}

export function FormFieldLayout(props: FormFieldLayoutProps) {
  return (
    <Form.Group className="mb-3" controlId={props.fieldName}>
      <Form.Label>{props.title}</Form.Label>
      <InputGroup hasValidation>
        <InputGroup.Text
          id={`inputGroupAppend_${props.fieldName}`}
          className="bg-slateblue"
        >
          {props.icon}
        </InputGroup.Text>
        {props.children}
        <InputGroup.Text id={`inputGroupAppend_${props.fieldName}`}>
          {props.valid ? (
            <Check size="24" color="lime" />
          ) : (
            <X size="24" color="red" />
          )}
        </InputGroup.Text>
      </InputGroup>
      <Form.Text className="text-muted">{props.description}</Form.Text>
      {props.formText}
    </Form.Group>
  );
}

export default FormFieldLayout;
