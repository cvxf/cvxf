import styles from './navigation.module.css';

import Link from 'next/link';
import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import { Settings, Save } from 'react-feather';
import { FileText, Gitlab } from 'react-feather';

/* eslint-disable-next-line */
export interface NavigationProps {}

export function Navigation(props: NavigationProps) {
  return (
    <Navbar bg="dark" variant="dark" fixed="top">
      <Container fluid>
        <Navbar.Brand>
          <Link href="/" className="text-white nav-link">
            <span className="fs-4">
              &nbsp;
              <FileText size={32} color="slateblue" />
              &nbsp;cvxf
            </span>
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link href="#">
              <Save size={17} color="slateblue" />
              &nbsp; Open/Save
            </Nav.Link>
            <Nav.Link href="#">
              <Settings size={17} color="slateblue" />
              &nbsp; Settings
            </Nav.Link>
            <Nav.Link href="https://gitlab.com/cvxf/cvxf">
              <Gitlab size={17} color="slateblue" />
              &nbsp; GitLab.com
            </Nav.Link>
            &nbsp; &nbsp; &nbsp;
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Navigation;
