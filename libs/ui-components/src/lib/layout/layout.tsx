import { ReactNode } from 'react';

import Sidebar from '../sidebar/sidebar';
import styles from './layout.module.css';

import { Container, Row, Col } from 'react-bootstrap';

/* eslint-disable-next-line */
export interface LayoutProps {
  children: ReactNode;
  sidebarLinks: ReactNode;
}

export function Layout(props: LayoutProps) {
  console.log('Creating component layout');

  return (
    <Container fluid className="p-0 app">
      <Row>
        <Col sm={2} xs={4} className="bg-light">
          <div className="fixed-sidebar col-sm-2 col-4">
            <Sidebar>{props.sidebarLinks}</Sidebar>
          </div>
        </Col>
        <Col sm={8} xs={4} className="p-0 h-scroll">
          <Row>
            <Col className="p-5">{props.children}</Col>
          </Row>
        </Col>
        <Col sm={2} xs={4}></Col>
      </Row>
    </Container>
  );
}

export default Layout;
