import { Container, Nav } from 'react-bootstrap';
import styles from './sidebar.module.css';

import { ReactNode } from 'react';

/* eslint-disable-next-line */
export interface SidebarProps {
  children: ReactNode;
}

export function Sidebar(props: SidebarProps) {
  return (
    <Container className="sidebar p-3 text-black">{props.children}</Container>
  );
}

export default Sidebar;
