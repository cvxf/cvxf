import styles from './form-string-field.module.css';

import {
  ReactNode,
  useState,
  useEffect,
  useLayoutEffect,
  KeyboardEvent,
} from 'react';
import { Form } from 'react-bootstrap';

import FormFieldLayout from '../form-field-layout/form-field-layout';

/* eslint-disable-next-line */
export interface FormStringFieldProps {
  title: string;
  namespace: string;
  fieldName: string;
  description: string;
  children: ReactNode;
  required?: boolean;
  multiline?: boolean;
}

export function FormStringField(props: FormStringFieldProps) {
  const storageStateName = `state${props.namespace}.${props.fieldName}`;

  const isInvalid = () => {
    if (props.required && !stringValue) {
      return true;
    }
    return false;
  };

  const asValue = props.multiline ? 'textarea' : 'input';
  const rows = props.multiline ? 3 : 1;

  const [stringValue, setstringValue] = useState('');

  useLayoutEffect(() => {
    if (sessionStorage.getItem(storageStateName)) {
      setstringValue(sessionStorage.getItem(storageStateName) || '');
    } else {
      sessionStorage.setItem(storageStateName, stringValue);
    }
  }, []);

  useEffect(() => {
    sessionStorage.setItem(storageStateName, stringValue);
  }, [stringValue]);

  const keyup = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      e.preventDefault();
    }
    return e;
  };

  return (
    <FormFieldLayout
      icon={props.children}
      title={props.title}
      description={props.description}
      fieldName={props.fieldName}
      valid={!isInvalid()}
    >
      {asValue === 'textarea' && (
        <Form.Control
          type=""
          placeholder={`Enter ${props.title}`}
          value={stringValue}
          onChange={(e) => {
            setstringValue(e.target.value);
          }}
          onKeyUp={(e: KeyboardEvent<HTMLElement>) => {
            return keyup(e);
          }}
          as={asValue}
          rows={rows}
          isInvalid={isInvalid()}
          required={props.required}
        />
      )}
      {asValue !== 'textarea' && (
        <Form.Control
          type=""
          placeholder={`Enter ${props.title}`}
          value={stringValue}
          onChange={(e) => {
            setstringValue(e.target.value);
          }}
          onKeyUp={(e: KeyboardEvent<HTMLElement>) => {
            return keyup(e);
          }}
          isInvalid={isInvalid()}
          required={props.required}
        />
      )}
    </FormFieldLayout>
  );
}

export default FormStringField;
