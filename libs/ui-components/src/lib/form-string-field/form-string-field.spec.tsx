import { render } from '@testing-library/react';

import FormStringField from './form-string-field';

describe('FormStringField', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FormStringField />);
    expect(baseElement).toBeTruthy();
  });
});
