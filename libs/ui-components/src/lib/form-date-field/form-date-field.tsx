import styles from './form-date-field.module.css';

/* eslint-disable-next-line */
import { ReactNode, useState, useEffect, useLayoutEffect } from 'react';
import { Form } from 'react-bootstrap';

import FormFieldLayout from '../form-field-layout/form-field-layout';
import { DatePicker } from '../date-picker/date-picker';
import { makeInputValueFromString } from '../date-picker/make-input-string-value';

/* eslint-disable-next-line */
export interface FormDateFieldProps {
  title: string;
  namespace: string;
  fieldName: string;
  description: string;
  children: ReactNode;
  required?: boolean;
  multiline?: boolean;
}

export function FormDateField(props: FormDateFieldProps) {
  const storageStateName = `state${props.namespace}.${props.fieldName}`;

  const isInvalid = () => {
    if (props.required && !dateValue) {
      return true;
    }
    return false;
  };

  const [dateValue, setDateValue] = useState('');

  useLayoutEffect(() => {
    if (sessionStorage.getItem(storageStateName)) {
      setDateValue(sessionStorage.getItem(storageStateName) || '');
    } else {
      sessionStorage.setItem(storageStateName, dateValue);
    }
  }, []);

  useEffect(() => {
    sessionStorage.setItem(storageStateName, dateValue);
  }, [dateValue]);

  return (
    <FormFieldLayout
      icon={props.children}
      title={props.title}
      description={props.description}
      fieldName={props.fieldName}
      valid={!isInvalid()}
    >
      <DatePicker
        onChange={(isoString?: string, date?: string) => {
          if (date) {
            setDateValue(date);
          }
        }}
        value={dateValue}
        placeholder="Date Field"
      />
    </FormFieldLayout>
  );
}

export default FormDateField;
