import { render } from '@testing-library/react';

import FormDateField from './form-date-field';

describe('FormDateField', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FormDateField />);
    expect(baseElement).toBeTruthy();
  });
});
