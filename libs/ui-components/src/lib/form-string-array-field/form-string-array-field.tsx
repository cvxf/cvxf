import styles from './form-string-array-field.module.css';

import {
  ReactNode,
  useState,
  useEffect,
  useLayoutEffect,
  KeyboardEvent,
  MouseEvent,
} from 'react';
import { Form } from 'react-bootstrap';

import { X } from 'react-feather';

import FormFieldLayout from '../form-field-layout/form-field-layout';

/* eslint-disable-next-line */
export interface FormStringArrayFieldProps {
  title: string;
  namespace: string;
  fieldName: string;
  description: string;
  children: ReactNode;
}

export function FormStringArrayField(props: FormStringArrayFieldProps) {
  const storageStateName = `state${props.namespace}.${props.fieldName}`;
  const tempStateName = `tmp_${storageStateName}`;

  const isInvalid = () => {
    return false;
  };

  const [stringValue, setStringValue] = useState('');

  const itemsList: string[] = [];
  const [arrayValue, setArrayValue] = useState(itemsList);

  useLayoutEffect(() => {
    if (sessionStorage.getItem(storageStateName)) {
      setStringValue(sessionStorage.getItem(tempStateName) || '');
      setArrayValue(sessionStorage.getItem(storageStateName)?.split(',') || []);
    } else {
      sessionStorage.setItem(tempStateName, stringValue);
      sessionStorage.setItem(storageStateName, arrayValue.join(','));
    }
  }, []);

  useEffect(() => {
    sessionStorage.setItem(tempStateName, stringValue);
    sessionStorage.setItem(storageStateName, arrayValue.join(','));
  }, [stringValue, arrayValue, tempStateName, storageStateName]);

  const keydown = (e: KeyboardEvent) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      setArrayValue(arrayValue.concat(stringValue.split(',')));
      setStringValue('');
    }
    return e;
  };

  const removeItem = (e: MouseEvent, removeItem: string) => {
    e.preventDefault();
    setArrayValue(arrayValue.filter((elm: string) => elm !== removeItem));
  };

  return (
    <FormFieldLayout
      icon={props.children}
      title={props.title}
      description={props.description}
      fieldName={props.fieldName}
      valid={!isInvalid()}
      formText=<Form.Text as="p">
        {arrayValue.map((arrayItem) => {
          return (
            <>
              <span key={arrayItem} className="badge bg-slateblue">
                <span className=" text-larger">
                  {arrayItem}{' '}
                  <a
                    href=""
                    onClick={(e: MouseEvent<HTMLElement>) =>
                      removeItem(e, arrayItem)
                    }
                    className="text-white"
                  >
                    <X size={16} />
                  </a>
                </span>
              </span>
              &nbsp;
            </>
          );
        })}
      </Form.Text>
    >
      <Form.Control
        type=""
        placeholder={`Enter ${props.title}: TIP you commas to sperate multiple ${props.title}`}
        value={stringValue}
        onChange={(e) => {
          setStringValue(e.target.value);
        }}
        onKeyDown={keydown}
        isInvalid={isInvalid()}
      />
    </FormFieldLayout>
  );
}

export default FormStringArrayField;
