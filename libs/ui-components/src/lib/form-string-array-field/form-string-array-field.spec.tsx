import { render } from '@testing-library/react';

import FormStringArrayField from './form-string-array-field';

describe('FormStringArrayField', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FormStringArrayField />);
    expect(baseElement).toBeTruthy();
  });
});
