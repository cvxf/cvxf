import * as fs from 'fs';

export function utils(): string {
  return 'utils';
}

export function readTextFile(filePath: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export function readJsonFile(filePath: string): Promise<any> {
  return readTextFile(filePath).then((data) => {
    return JSON.parse(data);
  });
}

export function saveFile(filePath: string, data: string): Promise<any> {
  return new Promise((resolve, reject) => {
    fs.writeFile(filePath, data, 'utf8', (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export function saveJsonFile(filePath: string, data: object): Promise<any> {
  return saveFile(filePath, JSON.stringify(data, null, 2));
}
